import time

def readFile(path):
    f = open(path, "r")
    content = f.read()
    f.close()
    return content

cpu_share = int(readFile("/sys/fs/cgroup/cpu/cpu.shares"))
n_core = cpu_share/1024

def getCpuUsage():
    time_initial = time.time()*(10**9)
    cpu_initial = int(readFile("/sys/fs/cgroup/cpu/cpuacct.usage"))
    time.sleep(2)
    time_final = time.time()*(10**9)
    cpu_final = int(readFile("/sys/fs/cgroup/cpu/cpuacct.usage"))

    cpu_usage = (cpu_final-cpu_initial)/(time_final-time_initial)
    avg_cpu_usage = cpu_usage/n_core

    return int(avg_cpu_usage*100)

def getMemoryUsage():
    total_mem = int(readFile("/sys/fs/cgroup/memory/memory.limit_in_bytes"))
    used_mem = int(readFile("/sys/fs/cgroup/memory/memory.usage_in_bytes"))
    return int((used_mem*100)/total_mem)