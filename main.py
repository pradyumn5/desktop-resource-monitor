import os
import threading
from tkinter import *
from tkinter.ttk import *
from PIL import Image, ImageTk
from utils import getCpuUsage, getMemoryUsage

root = Tk()
root.configure(background="#CCCCCC", highlightthickness=2)
icon = PhotoImage(file="./assets/icon.png")
root.iconphoto(False, icon)

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

root.geometry("{}x{}+{}+{}".format(360, 110, screen_width-360-50, screen_height-110-100))

style = Style()
style.configure("green.Horizontal.TProgressbar", foreground='#00994C', background='#00994C', troughcolor="#EAE8DB")
style.configure("yellow.Horizontal.TProgressbar", foreground='#EFB700', background='#EFB700', troughcolor="#EAE8DB")
style.configure("red.Horizontal.TProgressbar", foreground='#B81D13', background='#B81D13', troughcolor="#EAE8DB")

cpu_label = Label(root, text = "CPU", background="#CCCCCC")

cpu_img = Image.open("./assets/cpu.png")
cpu_img = cpu_img.resize((24, 24), Image.ANTIALIAS)
cpu_img =  ImageTk.PhotoImage(cpu_img)
cpu_icon = Canvas(root, width=26, height=26, background="#CCCCCC", borderwidth=0, highlightthickness=0)
cpu_icon.create_image(13, 13, image=cpu_img)

cpu_percent = Label(root, text = "00%", background="#CCCCCC")
cpu_bar = Progressbar(root, orient=HORIZONTAL, length=220, mode='determinate', style="green.Horizontal.TProgressbar")

memory_label = Label(root, text = "RAM", background="#CCCCCC")

memory_img = Image.open("./assets/ram.png")
memory_img = memory_img.resize((24, 24), Image.ANTIALIAS)
memory_img =  ImageTk.PhotoImage(memory_img)
memory_icon = Canvas(root, width=26, height=26, background="#CCCCCC", borderwidth=0, highlightthickness=0)
memory_icon.create_image(13, 13, image=memory_img)

memory_percent = Label(root, text = "00%", background="#CCCCCC")
memory_bar = Progressbar(root, orient=HORIZONTAL, length=220, mode='determinate', style="green.Horizontal.TProgressbar")

cpu_icon.grid(row=0, column=0, padx=10, pady=5)
cpu_label.grid(row=0, column=1, padx=2, pady=5)
cpu_percent.grid(row=0, column=2, padx=2, pady=5)
cpu_bar.grid(row=0, column=3, padx=10, pady=5)

memory_icon.grid(row=1, column=0, padx=10, pady=5)
memory_label.grid(row=1, column=1, padx=2, pady=5)
memory_percent.grid(row=1, column=2, padx=2, pady=5)
memory_bar.grid(row=1, column=3, padx=10, pady=5)

message_block = Message(root, text="Machine Health: OK", foreground="#00994C", background="#CCCCCC", width=300, 
                            font="Monospace 10 bold")
message_block.grid(row=2, column=0, columnspan=4, sticky="w", padx=5, pady=5)

show_warning = BooleanVar(root)
show_warning.set(True)
warning_open = BooleanVar(root)
warning_open.set(False)

def popup(message):
    warning_open.set(True)
    os.system("zenity --warning --title='{}' --text='{}'".format("System Warning", message))
    warning_open.set(False)
    
def updateResourceVal():
    cpu_usage = getCpuUsage()
    memory_usage = getMemoryUsage()

    cpu_percent["text"] = str(cpu_usage)+"%"
    cpu_bar["value"] = cpu_usage

    memory_percent["text"] = str(memory_usage)+"%"
    memory_bar["value"] = memory_usage
    
    message = "Machine Health: OK"
    color = "#00994C"

    if cpu_usage >= 80:
        message = "Machine Running Low On CPU"
        color = "#B81D13"
        cpu_bar["style"] = "red.Horizontal.TProgressbar"
    elif cpu_usage >= 60:
        cpu_bar["style"] = "yellow.Horizontal.TProgressbar"
    else:
        cpu_bar["style"] = "green.Horizontal.TProgressbar"

    if memory_usage >= 80:
        message = "Machine Running Low On Memory"
        color = "#B81D13"
        if cpu_usage >= 80:
            message = "Machine Running Low On CPU & Memory"
        memory_bar["style"] = "red.Horizontal.TProgressbar"
    elif memory_usage >= 60:
        memory_bar["style"] = "yellow.Horizontal.TProgressbar"
    else:
        memory_bar["style"] = "green.Horizontal.TProgressbar"
        if cpu_usage <= 50:
            show_warning.set(True)

    message_block["text"] = message
    message_block["foreground"] = color

    if not warning_open.get() and show_warning.get() and message != "Machine Health: OK":
        warn_thread = threading.Thread(target=popup, args=(message,))
        warn_thread.start()
        show_warning.set(False)
    
    root.after(2000, updateResourceVal)

root.title("Resource Monitor")
root.resizable(False, False)
root.after(0, updateResourceVal)
root.mainloop()